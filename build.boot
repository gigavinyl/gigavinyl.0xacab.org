(set-env!
 :source-paths #{"content"}
 :dependencies '[[perun "0.4.3-SNAPSHOT" :scope "test"]])

(require '[io.perun :as perun])
